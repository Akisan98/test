export async function getUserByName(username) {
    const response = await fetch(`https://noroff-assignment-5-api.herokuapp.com/translations?username=${username}`)
    return await response.json();
}

export async function postUser(username, translations) {
    const response = await fetch(
        `https://noroff-assignment-5-api.herokuapp.com/translations`,
        {
            method: 'POST',
            headers: {
                'X-API-Key': 'biq?BnhbCE&h3A8dpTz8p6!?NAf&HtEx3yE3TnosGcoH&hRKPDRPDhBmn!J$5Y79',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                translations: translations
            })
        }
    )
    return await response.json()
}

export async function patchUser(userID, translations) {
    const response = await fetch(
        `https://noroff-assignment-5-api.herokuapp.com/translations/${userID}`,
        {
            method: 'PATCH',
            headers: {
                'X-API-Key': 'biq?BnhbCE&h3A8dpTz8p6!?NAf&HtEx3yE3TnosGcoH&hRKPDRPDhBmn!J$5Y79',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                translations: translations
            })
        }
    )
    return await response.json()
}
/*

export function deleteTranslations(username) {
    const resposne = await fetch(`http://noroff-api-f.herokuapp.com/translations?username=${username}`, {
        method: 'DELETE',
        headers: {
            'x-api-key': api_key,
        },
        body: JSON.stringify(data),
    });
    return response.json();
}

export function addTranslation(username) {
    const resposne = await fetch(`http://noroff-api-f.herokuapp.com/translations?username=${username}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'x-api-key': api_key,
        },
        body: JSON.stringify(data),
    });
    return response.json();
}
*/

export const TranslationAPI = {
    login(username) {
        return fetch (`https://noroff-api-f.herokuapp.com/translations?username=${username}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': "x38sWruratOx5CzPpHGP0r1ugjshOizIwGdgyNvK521Bd89oefmBbQWckJyGoiAT",
            },
            body: JSON.stringify(username)
        })
        .then( async (response) => {
            if (!response.ok) {
                //const { error = 'could not login' } = await response.json()
                throw new Error('could not login')
            }
            return response.json()
        })
    }
}