import React, {useEffect} from 'react'
import styles from "./style/profile.module.css"
import {useTranslationContext} from "../contexts/TranslationContext";
import withUser from "../hoc/withUser";
import {patchUser} from "../api/translations_API";
import {useUserContext} from "../contexts/UserContext";
import {useHistory} from "react-router-dom";

const Profile = () => {

    console.log('Profile.render')
    const { translationState, dispatch } = useTranslationContext()
    const [ user, setUser ] = useUserContext()
    const history = useHistory()

    const clearSearchLog = () => {
        let newPayload = translationState.translation[0];
        newPayload.translations = [];
        dispatch({ type: 'SET_TRANSLATION', payload: [newPayload]})
        patchUser(translationState.translation[0].id, newPayload.translations)
    }

    const logOut = () => {
        // Reset Data
        dispatch({ type: 'RESET_ALL' });

        // Reset User
        setUser('');

        // Navigate To Login
        history.push("/")
    }

    return (
        <>
          <main>
              <h2>History: </h2>
              { translationState.loading && <p>Loading guitars...</p> }
              { translationState.translation.length > 0 &&
              <ul className={styles.historyList}>
                  { translationState.translation[0].translations.map((searches, index) =>  <li key={ index }>{ searches }</li>)}
              </ul>
              }

              <button className={styles.clearButton} onClick={ clearSearchLog }>
                  Clear History
                  <i class="fas fa-eraser"></i>                  </button>
              <button className={styles.logoutButton} onClick={ logOut }>Log Out</button>
          </main>  
        </>
    )
}

export default withUser(Profile)
