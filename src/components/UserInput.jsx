import React from 'react'
import styles from './style/UserInput.module.css'

const UserInput = ({inputOnChange, buttonOnClick, placeholderText, buttonText}) => {
    return (
        <div className={styles.UserInput}>
            <input className={styles.TextInput} onChange={inputOnChange} placeholder={placeholderText}></input>
            <button className={styles.SubmitButton} onClick={buttonOnClick}>
                <div className={styles.buttonText}>{ "TEST!" }</div>
                <i className="fas fa-arrow-right"></i>
            </button>
        </div>
    )
}

export default UserInput
