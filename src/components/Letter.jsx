import React from 'react'

const Letter = (props) => {

    const boxStyle = {
        color: "green",
        alignContent: "center",
        justifyContent: "center",
        width: "50px",
        margin: "0"
    }

    const imgStyle = {
        height: "50px",
        width: "50px"
    }

    return (
        <div style={boxStyle}>
            <img src={props.img} style={imgStyle} width="150px" height="150px"/>
            <p>{props.text[props.number]}</p>
        </div>
    )
}

export default Letter
