import React, {useEffect, useState} from 'react'
import Letter from './Letter';
import styles from './style/translator.module.css';
import UserInput from './UserInput';
import {useTranslationContext} from "../contexts/TranslationContext";
import {patchUser} from "../api/translations_API";

export const Translator = () => {
  const[search, setSearch] = useState("");
  const[letters, setLetters] = useState([]);
  const { translationState, dispatch } = useTranslationContext();

  const updateSearchHistory = async (query) => {
      console.log(query)
      console.log(translationState)
      console.log(translationState.translation[0])
      console.log(translationState.translation[0].translations)
      console.log(translationState.translation[0].translations.length)

      if (translationState.translation[0].translations.length === 0) {
          console.log("User Has No Log!")

          let newPayload = translationState.translation[0];
          newPayload.translations = [query];
          dispatch({ type: 'SET_TRANSLATION', payload: [newPayload]})

          //console.log(translationState.translation)
          console.log([newPayload])
      } else {
          console.log("User Has History!")

          let newPayload = translationState.translation[0];
          if (newPayload.translations.length === 10) newPayload.translations = newPayload.translations.slice(1)
          newPayload.translations.push(query);
          dispatch({ type: 'SET_TRANSLATION', payload: [newPayload]})

          //console.log(translationState.translation)
          console.log([newPayload])

      }

      //console.log(translationState.translation)
      //console.log(translationState.translation[0])
      console.log(translationState.translation[0].translations)
      // Upload Data
      patchUser(translationState.translation[0].id, translationState.translation[0].translations)
  }

  const handleChange = (event) => {
    setSearch(event.target.value.replace(/\W|\d/g, ""))
  }

  const textToASL = () => {
    setLetters([]);
    let imageArray = [];

    for (const letter of search) {
      imageArray.push(`/lostInTranslation/individial_signs/${letter}.png`)
      setLetters(imageArray)
    }

    updateSearchHistory(search)
  }

  return (
    <div className={styles.translatorContainer}>
      <div className={styles.inputContainer}>
        <UserInput inputOnChange={ handleChange } buttonOnClick={ textToASL } placeholderText={"Enter Text To Translate"} buttonText={"Translate"}/>
      </div>
      <br/>
      <br/>
      <br/>
      <div className={styles.outputContainer}>
        {}
      {letters.map((image, index) => <img src={image} key={index} width="150px" height="150px"/>)}
      {/* <Letter img={image} text={search} number={index} key={index}></Letter> */}
      </div>
    </div>
  )
}