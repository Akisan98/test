import React from 'react'
import styles from "./style/header.module.css"

export const Header = () => {
    return (
        <>
        <header>
            <a className={styles.logoContainer}>
                <img className={styles.logoImg} src="/lostInTranslation/Logo.png" width="50px" height="50px"  alt="Logo Image"/>
                <h2 className={styles.logoTitle}>Lost in Translation</h2>
            </a>
            <div className={styles.headerProfileDisplay}>
                <h3 className={styles.profileName}>User</h3>
                <i class="fas fa-user-circle fa-2x"></i>
            </div>
        </header>   
        </>
    )
}
