import React, {Component} from "react";
import {Redirect} from "react-router-dom";
import {useUserContext} from "../contexts/UserContext";

const withUser = Component => props => {
    const { user } = useUserContext()
    console.log(user)

    if (user !== '') {
        console.log("No Redir")
        return <Component {...props} />
    } else {
        console.log("Redir")
        return  <Redirect to="/" />
    }
}

export default withUser
