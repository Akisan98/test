import React from "react";
import {Redirect, Route} from "react-router-dom";
import {useUserContext} from "../contexts/UserContext";

const ProtectedRoute = ({component, path}) => {
    const [ user ] = useUserContext()

    console.log(`USER: ${user}`)
    console.log(user !== '' && user !== undefined)

    return user !== '' && user !== undefined
        ? <Route component={component} path={path}/>
        : <Redirect to="/"/>
}

export default ProtectedRoute